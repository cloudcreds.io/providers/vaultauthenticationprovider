package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/hashicorp/go-plugin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	plugs "gitlab.com/cloudcreds.io/cloudcredentials/pkg/provider"
	"io/ioutil"
	"net/http"
	"os"
)

type AuthenticationProvider struct {
	log *zerolog.Logger
}

func NewAuthenticationProvider(logger *zerolog.Logger) AuthenticationProvider {
	return AuthenticationProvider{log: logger}
}

type LoginCredentials struct {
	LeaseID       string      `json:"lease_id"`
	Renewable     bool        `json:"renewable"`
	LeaseDuration int         `json:"lease_duration"`
	Data          interface{} `json:"data"`
	Auth          struct {
		ClientToken string   `json:"client_token"`
		Policies    []string `json:"policies"`
		Metadata    struct {
			Username string `json:"username"`
		} `json:"metadata"`
		LeaseDuration int  `json:"lease_duration"`
		Renewable     bool `json:"renewable"`
	} `json:"auth"`
}

func (vault AuthenticationProvider) Authenticate(input plugs.AuthenticationProviderInput) []byte {
	baseURL := input.Request["BASE_URL"]
	var url string
	var payload = make(map[string]string)
	switch input.Method {
	case "uname":
		log.Debug().Msg("Logging in via username/password")
		url = fmt.Sprintf("%s/v1/auth/userpass/login/%s", baseURL, input.Request["USERNAME"])
		payload["password"] = fmt.Sprintf("%s", input.Request["PASSWORD"])
	case "ldap":
		log.Debug().Msg("Logging in via ldap")
		url = fmt.Sprintf("%s/v1/auth/ldap/login/%s", baseURL, input.Request["USERNAME"])
		payload["password"] = fmt.Sprintf("%s", input.Request["PASSWORD"])
	default:
		return nil
	}
	log.Debug().Str("url", url).Msg("Vault URL")
	byts, _ := json.Marshal(payload)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(byts))
	if err != nil {
		return nil
	}
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil
	}
	var lc LoginCredentials
	_ = json.Unmarshal(body, &lc)
	return []byte(lc.Auth.ClientToken)

}

var handshakeConfig = plugin.HandshakeConfig{
	ProtocolVersion:  1,
	MagicCookieKey:   "BASIC_PLUGIN",
	MagicCookieValue: "hello",
}

func main() {
	logger := zerolog.New(os.Stderr).With().Timestamp().Logger()
	vault := NewAuthenticationProvider(&logger)

	var pluginMap = map[string]plugin.Plugin{
		"auth": &plugs.AuthenticationProviderPlugin{Impl: vault},
	}

	plugin.Serve(&plugin.ServeConfig{
		HandshakeConfig: handshakeConfig,
		Plugins:         pluginMap,
	})
}
