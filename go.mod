module gitlab.com/cloudcreds.io/vaultauthenticationprovider

go 1.14

require (
	github.com/fatih/color v1.9.0 // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/google/go-cmp v0.4.1 // indirect
	github.com/hashicorp/go-hclog v0.14.1 // indirect
	github.com/hashicorp/go-plugin v1.3.0
	github.com/hashicorp/yamux v0.0.0-20190923154419-df201c70410d // indirect
	github.com/jhump/protoreflect v1.6.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mitchellh/go-testing-interface v1.14.1 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/oklog/run v1.1.0 // indirect
	github.com/rs/zerolog v1.19.0
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/zenazn/goji v0.9.0 // indirect
	gitlab.com/cloudcreds.io/cloudcredentials v0.0.0-20200607210011-1526ec6af16b
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9 // indirect
	golang.org/x/sys v0.0.0-20200602225109-6fdc65e7d980 // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/genproto v0.0.0-20200605102947-12044bf5ea91 // indirect
	google.golang.org/grpc v1.29.1 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v3 v3.0.0-20200605160147-a5ece683394c // indirect
)

// replace gitlab.com/cloudcreds.io/cloudcredentials => /Users/polliard/workspace/go/src/gitlab.com/cloudcreds.io/cloudcredentials
